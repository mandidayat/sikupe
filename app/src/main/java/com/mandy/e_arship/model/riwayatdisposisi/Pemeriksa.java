package com.mandy.e_arship.model.riwayatdisposisi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pemeriksa {

    @SerializedName("jab_name")
    @Expose
    private String jabName;
    @SerializedName("disposisi_valid_at")
    @Expose
    private String disposisiValidAt;

    public String getJabName() {
        return jabName;
    }

    public void setJabName(String jabName) {
        this.jabName = jabName;
    }

    public String getDisposisiValidAt() {
        return disposisiValidAt;
    }

    public void setDisposisiValidAt(String disposisiValidAt) {
        this.disposisiValidAt = disposisiValidAt;
    }
}
