package com.mandy.e_arship.model.notif;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class get_notif {
    @SerializedName("pesan")
    @Expose
    private String pesan;
    @SerializedName("total_data")
    @Expose
    private Integer totalData;
    @SerializedName("notif")
    @Expose
    private List<notif_data> notif = null;

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public Integer getTotalData() {
        return totalData;
    }

    public void setTotalData(Integer totalData) {
        this.totalData = totalData;
    }

    public List<notif_data> getNotif() {
        return notif;
    }

    public void setNotif(List<notif_data> notif) {
        this.notif = notif;
    }

}
