package com.mandy.e_arship.model.riwayatdisposisi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RIwayatDisposisiResponse {
    @SerializedName("pesan")
    @Expose
    private String pesan;
    @SerializedName("total_data")
    @Expose
    private Integer totalData;
    @SerializedName("riwayat_diposisi")
    @Expose
    private List<RiwayatDiposisiData> riwayatDiposisi = null;

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public Integer getTotalData() {
        return totalData;
    }

    public void setTotalData(Integer totalData) {
        this.totalData = totalData;
    }

    public List<RiwayatDiposisiData> getRiwayatDiposisi() {
        return riwayatDiposisi;
    }

    public void setRiwayatDiposisi(List<RiwayatDiposisiData> riwayatDiposisi) {
        this.riwayatDiposisi = riwayatDiposisi;
    }

}
