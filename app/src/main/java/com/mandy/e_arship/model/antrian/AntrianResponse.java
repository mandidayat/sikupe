package com.mandy.e_arship.model.antrian;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AntrianResponse {
    @SerializedName("pesan")
    @Expose
    private String pesan;

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

}
