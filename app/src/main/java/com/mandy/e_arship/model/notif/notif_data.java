package com.mandy.e_arship.model.notif;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class notif_data {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("create")
    @Expose
    private String create;
    @SerializedName("is_read")
    @Expose
    private String isRead;
    @SerializedName("to_user_id")
    @Expose
    private String toUserId;
    @SerializedName("from")
    @Expose
    private String from;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCreate() {
        return create;
    }

    public void setCreate(String create) {
        this.create = create;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

}
