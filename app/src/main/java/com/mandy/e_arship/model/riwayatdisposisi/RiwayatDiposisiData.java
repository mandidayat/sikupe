package com.mandy.e_arship.model.riwayatdisposisi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RiwayatDiposisiData {
    @SerializedName("id_disposisi")
    @Expose
    private String idDisposisi;
    @SerializedName("disposisi_intruksi")
    @Expose
    private String disposisiIntruksi;
    @SerializedName("disposisi_sifat_id")
    @Expose
    private String disposisiSifatId;
    @SerializedName("disposisi_dibuat_tanggal")
    @Expose
    private String disposisiDibuatTanggal;
    @SerializedName("disposisi_user_id")
    @Expose
    private String disposisiUserId;
    @SerializedName("disposisi_surat_index_id")
    @Expose
    private String disposisiSuratIndexId;
    @SerializedName("disposisi_batas_waktu")
    @Expose
    private String disposisiBatasWaktu;
    @SerializedName("sifat_id")
    @Expose
    private String sifatId;
    @SerializedName("sifat_nama")
    @Expose
    private String sifatNama;
    @SerializedName("sifat_keterangan")
    @Expose
    private String sifatKeterangan;
    @SerializedName("surat_id_index")
    @Expose
    private String suratIdIndex;
    @SerializedName("surat_no")
    @Expose
    private String suratNo;
    @SerializedName("surat_pengirim")
    @Expose
    private String suratPengirim;
    @SerializedName("surat_tanggal")
    @Expose
    private String suratTanggal;
    @SerializedName("surat_diterima_tanggal")
    @Expose
    private String suratDiterimaTanggal;
    @SerializedName("surat_perihal")
    @Expose
    private String suratPerihal;
    @SerializedName("surat_kategori_id")
    @Expose
    private String suratKategoriId;
    @SerializedName("surat_user_id")
    @Expose
    private String suratUserId;
    @SerializedName("surat_lampiran")
    @Expose
    private Object suratLampiran;
    @SerializedName("surat_kode")
    @Expose
    private String suratKode;
    @SerializedName("surat_keterangan")
    @Expose
    private Object suratKeterangan;
    @SerializedName("surat_dinas_id")
    @Expose
    private String suratDinasId;
    @SerializedName("surat_status")
    @Expose
    private String suratStatus;
    @SerializedName("surat_retensi_arsip")
    @Expose
    private Object suratRetensiArsip;
    @SerializedName("surat_diarsipkan_pada")
    @Expose
    private Object suratDiarsipkanPada;
    @SerializedName("surat_kode_penyimpanan")
    @Expose
    private Object suratKodePenyimpanan;
    @SerializedName("surat_instansi_id")
    @Expose
    private Object suratInstansiId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("ip_address")
    @Expose
    private String ipAddress;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("salt")
    @Expose
    private Object salt;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("activation_code")
    @Expose
    private Object activationCode;
    @SerializedName("forgotten_password_code")
    @Expose
    private Object forgottenPasswordCode;
    @SerializedName("forgotten_password_time")
    @Expose
    private Object forgottenPasswordTime;
    @SerializedName("remember_code")
    @Expose
    private Object rememberCode;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("last_login")
    @Expose
    private String lastLogin;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("pemeriksa")
    @Expose
    private List<Pemeriksa> pemeriksa = null;

    public String getIdDisposisi() {
        return idDisposisi;
    }

    public void setIdDisposisi(String idDisposisi) {
        this.idDisposisi = idDisposisi;
    }

    public String getDisposisiIntruksi() {
        return disposisiIntruksi;
    }

    public void setDisposisiIntruksi(String disposisiIntruksi) {
        this.disposisiIntruksi = disposisiIntruksi;
    }

    public String getDisposisiSifatId() {
        return disposisiSifatId;
    }

    public void setDisposisiSifatId(String disposisiSifatId) {
        this.disposisiSifatId = disposisiSifatId;
    }

    public String getDisposisiDibuatTanggal() {
        return disposisiDibuatTanggal;
    }

    public void setDisposisiDibuatTanggal(String disposisiDibuatTanggal) {
        this.disposisiDibuatTanggal = disposisiDibuatTanggal;
    }

    public String getDisposisiUserId() {
        return disposisiUserId;
    }

    public void setDisposisiUserId(String disposisiUserId) {
        this.disposisiUserId = disposisiUserId;
    }

    public String getDisposisiSuratIndexId() {
        return disposisiSuratIndexId;
    }

    public void setDisposisiSuratIndexId(String disposisiSuratIndexId) {
        this.disposisiSuratIndexId = disposisiSuratIndexId;
    }

    public String getDisposisiBatasWaktu() {
        return disposisiBatasWaktu;
    }

    public void setDisposisiBatasWaktu(String disposisiBatasWaktu) {
        this.disposisiBatasWaktu = disposisiBatasWaktu;
    }

    public String getSifatId() {
        return sifatId;
    }

    public void setSifatId(String sifatId) {
        this.sifatId = sifatId;
    }

    public String getSifatNama() {
        return sifatNama;
    }

    public void setSifatNama(String sifatNama) {
        this.sifatNama = sifatNama;
    }

    public String getSifatKeterangan() {
        return sifatKeterangan;
    }

    public void setSifatKeterangan(String sifatKeterangan) {
        this.sifatKeterangan = sifatKeterangan;
    }

    public String getSuratIdIndex() {
        return suratIdIndex;
    }

    public void setSuratIdIndex(String suratIdIndex) {
        this.suratIdIndex = suratIdIndex;
    }

    public String getSuratNo() {
        return suratNo;
    }

    public void setSuratNo(String suratNo) {
        this.suratNo = suratNo;
    }

    public String getSuratPengirim() {
        return suratPengirim;
    }

    public void setSuratPengirim(String suratPengirim) {
        this.suratPengirim = suratPengirim;
    }

    public String getSuratTanggal() {
        return suratTanggal;
    }

    public void setSuratTanggal(String suratTanggal) {
        this.suratTanggal = suratTanggal;
    }

    public String getSuratDiterimaTanggal() {
        return suratDiterimaTanggal;
    }

    public void setSuratDiterimaTanggal(String suratDiterimaTanggal) {
        this.suratDiterimaTanggal = suratDiterimaTanggal;
    }

    public String getSuratPerihal() {
        return suratPerihal;
    }

    public void setSuratPerihal(String suratPerihal) {
        this.suratPerihal = suratPerihal;
    }

    public String getSuratKategoriId() {
        return suratKategoriId;
    }

    public void setSuratKategoriId(String suratKategoriId) {
        this.suratKategoriId = suratKategoriId;
    }

    public String getSuratUserId() {
        return suratUserId;
    }

    public void setSuratUserId(String suratUserId) {
        this.suratUserId = suratUserId;
    }

    public Object getSuratLampiran() {
        return suratLampiran;
    }

    public void setSuratLampiran(Object suratLampiran) {
        this.suratLampiran = suratLampiran;
    }

    public String getSuratKode() {
        return suratKode;
    }

    public void setSuratKode(String suratKode) {
        this.suratKode = suratKode;
    }

    public Object getSuratKeterangan() {
        return suratKeterangan;
    }

    public void setSuratKeterangan(Object suratKeterangan) {
        this.suratKeterangan = suratKeterangan;
    }

    public String getSuratDinasId() {
        return suratDinasId;
    }

    public void setSuratDinasId(String suratDinasId) {
        this.suratDinasId = suratDinasId;
    }

    public String getSuratStatus() {
        return suratStatus;
    }

    public void setSuratStatus(String suratStatus) {
        this.suratStatus = suratStatus;
    }

    public Object getSuratRetensiArsip() {
        return suratRetensiArsip;
    }

    public void setSuratRetensiArsip(Object suratRetensiArsip) {
        this.suratRetensiArsip = suratRetensiArsip;
    }

    public Object getSuratDiarsipkanPada() {
        return suratDiarsipkanPada;
    }

    public void setSuratDiarsipkanPada(Object suratDiarsipkanPada) {
        this.suratDiarsipkanPada = suratDiarsipkanPada;
    }

    public Object getSuratKodePenyimpanan() {
        return suratKodePenyimpanan;
    }

    public void setSuratKodePenyimpanan(Object suratKodePenyimpanan) {
        this.suratKodePenyimpanan = suratKodePenyimpanan;
    }

    public Object getSuratInstansiId() {
        return suratInstansiId;
    }

    public void setSuratInstansiId(Object suratInstansiId) {
        this.suratInstansiId = suratInstansiId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Object getSalt() {
        return salt;
    }

    public void setSalt(Object salt) {
        this.salt = salt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(Object activationCode) {
        this.activationCode = activationCode;
    }

    public Object getForgottenPasswordCode() {
        return forgottenPasswordCode;
    }

    public void setForgottenPasswordCode(Object forgottenPasswordCode) {
        this.forgottenPasswordCode = forgottenPasswordCode;
    }

    public Object getForgottenPasswordTime() {
        return forgottenPasswordTime;
    }

    public void setForgottenPasswordTime(Object forgottenPasswordTime) {
        this.forgottenPasswordTime = forgottenPasswordTime;
    }

    public Object getRememberCode() {
        return rememberCode;
    }

    public void setRememberCode(Object rememberCode) {
        this.rememberCode = rememberCode;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Pemeriksa> getPemeriksa() {
        return pemeriksa;
    }

    public void setPemeriksa(List<Pemeriksa> pemeriksa) {
        this.pemeriksa = pemeriksa;
    }
}
