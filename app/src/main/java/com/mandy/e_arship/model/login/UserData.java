package com.mandy.e_arship.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {
    @SerializedName("id_user_opd")
    @Expose
    private String idUserOpd;
    @SerializedName("token_akses")
    @Expose
    private String tokenAkses;
    @SerializedName("user_opd_an")
    @Expose
    private String userOpdAn;
    @SerializedName("user_opd_nama")
    @Expose
    private String userOpdNama;
    @SerializedName("user_opd_telp")
    @Expose
    private String userOpdTelp;
    @SerializedName("token_firebase")
    @Expose
    private String tokenFirebase;
    @SerializedName("instansi_id")
    @Expose
    private String instansiId;
    @SerializedName("user_opd_stat")
    @Expose
    private String userOpdStat;
    @SerializedName("instansi_name")
    @Expose
    private String instansiName;
    @SerializedName("instansi_telepon")
    @Expose
    private String instansiTelepon;
    @SerializedName("instansi_alamat")
    @Expose
    private String instansiAlamat;
    @SerializedName("instansi_email")
    @Expose
    private String instansiEmail;
    @SerializedName("instansi_kategori_id")
    @Expose
    private String instansiKategoriId;
    @SerializedName("instansi_kode_unik")
    @Expose
    private String instansiKodeUnik;

    public String getIdUserOpd() {
        return idUserOpd;
    }

    public void setIdUserOpd(String idUserOpd) {
        this.idUserOpd = idUserOpd;
    }

    public String getTokenAkses() {
        return tokenAkses;
    }

    public void setTokenAkses(String tokenAkses) {
        this.tokenAkses = tokenAkses;
    }

    public String getUserOpdAn() {
        return userOpdAn;
    }

    public void setUserOpdAn(String userOpdAn) {
        this.userOpdAn = userOpdAn;
    }

    public String getUserOpdNama() {
        return userOpdNama;
    }

    public void setUserOpdNama(String userOpdNama) {
        this.userOpdNama = userOpdNama;
    }

    public String getUserOpdTelp() {
        return userOpdTelp;
    }

    public void setUserOpdTelp(String userOpdTelp) {
        this.userOpdTelp = userOpdTelp;
    }

    public String getTokenFirebase() {
        return tokenFirebase;
    }

    public void setTokenFirebase(String tokenFirebase) {
        this.tokenFirebase = tokenFirebase;
    }

    public String getInstansiId() {
        return instansiId;
    }

    public void setInstansiId(String instansiId) {
        this.instansiId = instansiId;
    }

    public String getUserOpdStat() {
        return userOpdStat;
    }

    public void setUserOpdStat(String userOpdStat) {
        this.userOpdStat = userOpdStat;
    }

    public String getInstansiName() {
        return instansiName;
    }

    public void setInstansiName(String instansiName) {
        this.instansiName = instansiName;
    }

    public String getInstansiTelepon() {
        return instansiTelepon;
    }

    public void setInstansiTelepon(String instansiTelepon) {
        this.instansiTelepon = instansiTelepon;
    }

    public String getInstansiAlamat() {
        return instansiAlamat;
    }

    public void setInstansiAlamat(String instansiAlamat) {
        this.instansiAlamat = instansiAlamat;
    }

    public String getInstansiEmail() {
        return instansiEmail;
    }

    public void setInstansiEmail(String instansiEmail) {
        this.instansiEmail = instansiEmail;
    }

    public String getInstansiKategoriId() {
        return instansiKategoriId;
    }

    public void setInstansiKategoriId(String instansiKategoriId) {
        this.instansiKategoriId = instansiKategoriId;
    }

    public String getInstansiKodeUnik() {
        return instansiKodeUnik;
    }

    public void setInstansiKodeUnik(String instansiKodeUnik) {
        this.instansiKodeUnik = instansiKodeUnik;
    }
}
