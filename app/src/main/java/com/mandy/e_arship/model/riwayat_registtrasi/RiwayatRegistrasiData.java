package com.mandy.e_arship.model.riwayat_registtrasi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RiwayatRegistrasiData {
    @SerializedName("id_antrian")
    @Expose
    private String idAntrian;
    @SerializedName("antrian_kode")
    @Expose
    private String antrianKode;
    @SerializedName("antrian_no_surat")
    @Expose
    private String antrianNoSurat;
    @SerializedName("antrian_tgl_surat")
    @Expose
    private String antrianTglSurat;
    @SerializedName("antrian_perihal")
    @Expose
    private String antrianPerihal;
    @SerializedName("antrian_tgl_waktu")
    @Expose
    private String antrianTglWaktu;
    @SerializedName("antrian_user_opd")
    @Expose
    private String antrianUserOpd;
    @SerializedName("antrian_status")
    @Expose
    private String antrianStatus;
    @SerializedName("surat_index")
    @Expose
    private Object suratIndex;
    @SerializedName("id_user_opd")
    @Expose
    private String idUserOpd;
    @SerializedName("token_akses")
    @Expose
    private String tokenAkses;
    @SerializedName("user_opd_an")
    @Expose
    private String userOpdAn;
    @SerializedName("user_opd_nama")
    @Expose
    private String userOpdNama;
    @SerializedName("user_opd_telp")
    @Expose
    private String userOpdTelp;
    @SerializedName("token_firebase")
    @Expose
    private String tokenFirebase;
    @SerializedName("instansi_id")
    @Expose
    private String instansiId;
    @SerializedName("user_opd_stat")
    @Expose
    private String userOpdStat;
    @SerializedName("instansi_name")
    @Expose
    private String instansiName;
    @SerializedName("instansi_telepon")
    @Expose
    private String instansiTelepon;
    @SerializedName("instansi_alamat")
    @Expose
    private String instansiAlamat;
    @SerializedName("instansi_email")
    @Expose
    private String instansiEmail;
    @SerializedName("instansi_kategori_id")
    @Expose
    private String instansiKategoriId;
    @SerializedName("instansi_kode_unik")
    @Expose
    private String instansiKodeUnik;

    public String getIdAntrian() {
        return idAntrian;
    }

    public void setIdAntrian(String idAntrian) {
        this.idAntrian = idAntrian;
    }

    public String getAntrianKode() {
        return antrianKode;
    }

    public void setAntrianKode(String antrianKode) {
        this.antrianKode = antrianKode;
    }

    public String getAntrianNoSurat() {
        return antrianNoSurat;
    }

    public void setAntrianNoSurat(String antrianNoSurat) {
        this.antrianNoSurat = antrianNoSurat;
    }

    public String getAntrianTglSurat() {
        return antrianTglSurat;
    }

    public void setAntrianTglSurat(String antrianTglSurat) {
        this.antrianTglSurat = antrianTglSurat;
    }

    public String getAntrianPerihal() {
        return antrianPerihal;
    }

    public void setAntrianPerihal(String antrianPerihal) {
        this.antrianPerihal = antrianPerihal;
    }

    public String getAntrianTglWaktu() {
        return antrianTglWaktu;
    }

    public void setAntrianTglWaktu(String antrianTglWaktu) {
        this.antrianTglWaktu = antrianTglWaktu;
    }

    public String getAntrianUserOpd() {
        return antrianUserOpd;
    }

    public void setAntrianUserOpd(String antrianUserOpd) {
        this.antrianUserOpd = antrianUserOpd;
    }

    public String getAntrianStatus() {
        return antrianStatus;
    }

    public void setAntrianStatus(String antrianStatus) {
        this.antrianStatus = antrianStatus;
    }

    public Object getSuratIndex() {
        return suratIndex;
    }

    public void setSuratIndex(Object suratIndex) {
        this.suratIndex = suratIndex;
    }

    public String getIdUserOpd() {
        return idUserOpd;
    }

    public void setIdUserOpd(String idUserOpd) {
        this.idUserOpd = idUserOpd;
    }

    public String getTokenAkses() {
        return tokenAkses;
    }

    public void setTokenAkses(String tokenAkses) {
        this.tokenAkses = tokenAkses;
    }

    public String getUserOpdAn() {
        return userOpdAn;
    }

    public void setUserOpdAn(String userOpdAn) {
        this.userOpdAn = userOpdAn;
    }

    public String getUserOpdNama() {
        return userOpdNama;
    }

    public void setUserOpdNama(String userOpdNama) {
        this.userOpdNama = userOpdNama;
    }

    public String getUserOpdTelp() {
        return userOpdTelp;
    }

    public void setUserOpdTelp(String userOpdTelp) {
        this.userOpdTelp = userOpdTelp;
    }

    public String getTokenFirebase() {
        return tokenFirebase;
    }

    public void setTokenFirebase(String tokenFirebase) {
        this.tokenFirebase = tokenFirebase;
    }

    public String getInstansiId() {
        return instansiId;
    }

    public void setInstansiId(String instansiId) {
        this.instansiId = instansiId;
    }

    public String getUserOpdStat() {
        return userOpdStat;
    }

    public void setUserOpdStat(String userOpdStat) {
        this.userOpdStat = userOpdStat;
    }

    public String getInstansiName() {
        return instansiName;
    }

    public void setInstansiName(String instansiName) {
        this.instansiName = instansiName;
    }

    public String getInstansiTelepon() {
        return instansiTelepon;
    }

    public void setInstansiTelepon(String instansiTelepon) {
        this.instansiTelepon = instansiTelepon;
    }

    public String getInstansiAlamat() {
        return instansiAlamat;
    }

    public void setInstansiAlamat(String instansiAlamat) {
        this.instansiAlamat = instansiAlamat;
    }

    public String getInstansiEmail() {
        return instansiEmail;
    }

    public void setInstansiEmail(String instansiEmail) {
        this.instansiEmail = instansiEmail;
    }

    public String getInstansiKategoriId() {
        return instansiKategoriId;
    }

    public void setInstansiKategoriId(String instansiKategoriId) {
        this.instansiKategoriId = instansiKategoriId;
    }

    public String getInstansiKodeUnik() {
        return instansiKodeUnik;
    }

    public void setInstansiKodeUnik(String instansiKodeUnik) {
        this.instansiKodeUnik = instansiKodeUnik;
    }
}
