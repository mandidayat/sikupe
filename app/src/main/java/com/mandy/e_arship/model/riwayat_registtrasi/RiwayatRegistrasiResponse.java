package com.mandy.e_arship.model.riwayat_registtrasi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RiwayatRegistrasiResponse {
    @SerializedName("pesan")
    @Expose
    private String pesan;
    @SerializedName("total_data")
    @Expose
    private Integer totalData;
    @SerializedName("riwayat_registrasi")
    @Expose
    private List<RiwayatRegistrasiData> riwayatRegistrasi = null;

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public Integer getTotalData() {
        return totalData;
    }

    public void setTotalData(Integer totalData) {
        this.totalData = totalData;
    }

    public List<RiwayatRegistrasiData> getRiwayatRegistrasi() {
        return riwayatRegistrasi;
    }

    public void setRiwayatRegistrasi(List<RiwayatRegistrasiData> riwayatRegistrasi) {
        this.riwayatRegistrasi = riwayatRegistrasi;
    }
}
