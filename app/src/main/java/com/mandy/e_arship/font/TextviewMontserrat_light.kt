package com.mandy.e_arship.font

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView


class TextviewMontserrat_light : AppCompatTextView{

    constructor(context: Context) : super(context) {

        applyCustomFont(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        applyCustomFont(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        applyCustomFont(context)
    }

    private fun applyCustomFont(context: Context) {
        val customFont = Typeface.createFromAsset(context.getAssets(), "font/montserrat_light.ttf")
        this.setTypeface(customFont)
    }
}