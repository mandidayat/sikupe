package com.mandy.e_arship.font

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView


class TextviewMontserrat : AppCompatTextView{

    constructor(context: Context) : super(context) {

        applyCustomFont(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        applyCustomFont(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        applyCustomFont(context)
    }

    private fun applyCustomFont(context: Context) {
        val customFont = Typeface.createFromAsset(context.getAssets(), "font/roboto_regular.ttf")
        this.setTypeface(customFont)
    }
}