package com.mandy.e_arship.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.mandy.e_arship.R;
import com.mandy.e_arship.model.riwayat_registtrasi.RiwayatRegistrasiData;

import java.util.List;

public class adapter_riwayat_registrasi extends RecyclerView.Adapter<adapter_riwayat_registrasi.MyViewHolder> {

    private Context context;
    private Activity activity;
    private List<RiwayatRegistrasiData> riwayat_disposisi;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView noReg,waktuReg,noSurat,perihalSurat,tv_no;

        public MyViewHolder(View view) {
            super(view);
            context = view.getContext();

            noReg = view.findViewById(R.id.tv_no_reg);
            waktuReg = view.findViewById(R.id.tv_waktu_reg);
            noSurat = view.findViewById(R.id.tv_no_surat);
            perihalSurat = view.findViewById(R.id.tv_perihal_surat);
            tv_no = view.findViewById(R.id.tv_no);
        }
    }

    public void setSurat(List<RiwayatRegistrasiData> riwayat_disposisi) {
        this.riwayat_disposisi = riwayat_disposisi;
        notifyDataSetChanged();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_riwayat_registrasi, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final RiwayatRegistrasiData barang = riwayat_disposisi.get(position);
        holder.tv_no.setText(String.valueOf(position+1));
        holder.noReg.setText(barang.getAntrianKode());
        holder.noSurat.setText(barang.getAntrianNoSurat());
        holder.perihalSurat.setText(barang.getAntrianPerihal());
        holder.waktuReg.setText(barang.getAntrianTglWaktu());

    }

    @Override
    public int getItemCount() {
        return riwayat_disposisi.size();
    }

}
