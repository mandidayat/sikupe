package com.mandy.e_arship.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.akexorcist.roundcornerprogressbar.TextRoundCornerProgressBar;
import com.mandy.e_arship.R;
import com.mandy.e_arship.model.riwayatdisposisi.Pemeriksa;
import com.mandy.e_arship.model.riwayatdisposisi.RiwayatDiposisiData;

import java.util.ArrayList;
import java.util.List;

public class adapter_riwayat_disposisi extends RecyclerView.Adapter<adapter_riwayat_disposisi.MyViewHolder> {

    private Context context;
    private Activity activity;
    private List<RiwayatDiposisiData> riwayat_disposisi;
    private List<Pemeriksa> nama_pemeriksa = new ArrayList<>();
    private AdapterPemerilsa adapter_pemeriksa;
    private LinearLayoutManager mLinearLayoutManager;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView perihal,no,tgl,no_urut,pem,indexSurat;
        public RecyclerView recyclerView_pemeriksa;
        public TextRoundCornerProgressBar progressBar1;
        private LinearLayout linear;
        private ListView listView;

        @SuppressLint("WrongConstant")
        public MyViewHolder(View view) {
            super(view);
            context = view.getContext();
            tgl = view.findViewById(R.id.tgl_surat);
            indexSurat = view.findViewById(R.id.index_surat);
            perihal = view.findViewById(R.id.perihal);
            linear = view.findViewById(R.id.linear);
            no = view.findViewById(R.id.no_surat);
            pem = view.findViewById(R.id.tv_pem);
            no_urut = view.findViewById(R.id.tv_no_urut);
            recyclerView_pemeriksa = view.findViewById(R.id.recyclerView_pemeriksa);
            progressBar1 = view.findViewById(R.id.progressBar1);
            recyclerView_pemeriksa = view.findViewById(R.id.recyclerView_pemeriksa);
            adapter_pemeriksa = new AdapterPemerilsa();
            mLinearLayoutManager = new LinearLayoutManager(context);
            mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

            recyclerView_pemeriksa.setHasFixedSize(true);
            recyclerView_pemeriksa.setLayoutManager(mLinearLayoutManager);
//            recyclerView_pemeriksa.setItemAnimator(new SlideUpItemAnimator());
//            recyclerView_pemeriksa.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
//            recyclerView_pemeriksa.setAdapter(adapter_pemeriksa);

        }
    }

    public void setSurat(List<RiwayatDiposisiData> riwayat_disposisi) {
        this.riwayat_disposisi = riwayat_disposisi;
        notifyDataSetChanged();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_riwayat_diposisi, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final RiwayatDiposisiData barang = riwayat_disposisi.get(position);
        int prog=0;
        holder.no.setText(barang.getSuratNo());
        holder.tgl.setText(barang.getSuratTanggal());
        holder.indexSurat.setText(barang.getSuratIdIndex());
        holder.no_urut.setText(String.valueOf(position+1));
//        holder.no_urut.setText(barang.getId_disposisi());
        holder.perihal.setText(barang.getSuratPerihal());
        StringBuilder stringBuffer = new StringBuilder();
        adapter_pemeriksa.setSurat(barang.getPemeriksa());
        holder.recyclerView_pemeriksa.setAdapter(adapter_pemeriksa);

        for(int x=0; x<barang.getPemeriksa().size();x++){
            String pem;
            if (barang.getPemeriksa().get(x).getDisposisiValidAt() == null){
                pem = "Belum Verifikasi";
            }else {
                prog = prog + 100;
                pem = "Tanggal Verifikasi : " + barang.getPemeriksa().get(x).getDisposisiValidAt();
            }
            stringBuffer.append("\n"+barang.getPemeriksa().get(x).getJabName() + "\n" + pem + "\n");
//            listArray[position]= pemeriksa.getJab_name() + ", " + pemeriksa.getDisposisi_valid_at();
        }
//        holder.pem.setText("Pemeriksa \n"  +stringBuffer);
        holder.progressBar1.setMax(barang.getPemeriksa().size()*100);
        holder.progressBar1.setProgress(prog);
//        holder.progressBar1.setTextProgressMargin(prog);
        if ((prog / holder.progressBar1.getMax() * 100) == 1){
            holder.progressBar1.setProgressText("100 %");
        }else{
            holder.progressBar1.setProgressText(Math.round(prog / holder.progressBar1.getMax() * 100) + "%");
        }

    }

    @Override
    public int getItemCount() {
        return riwayat_disposisi.size();
    }

}
