package com.mandy.e_arship.adapter;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.mandy.e_arship.R;
import com.mandy.e_arship.model.riwayatdisposisi.Pemeriksa;

import java.util.List;

public class AdapterPemerilsa extends RecyclerView.Adapter<AdapterPemerilsa.MyViewHolder> {

    private Context context;
    private Activity activity;
    private List<Pemeriksa> notif_data;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nama,tgl;
        private ImageView img_cek;

        public MyViewHolder(View view) {
            super(view);
            context = view.getContext();
            nama = view.findViewById(R.id.tv_nama);
            tgl = view.findViewById(R.id.tv_tgl);
            img_cek = view.findViewById(R.id.img_cek);

        }
    }

    public void setSurat(List<Pemeriksa> riwayat_disposisi) {
        this.notif_data = riwayat_disposisi;
        notifyDataSetChanged();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pemeriksa, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Pemeriksa barang = notif_data.get(position);

        holder.nama.setText(barang.getJabName());
        holder.tgl.setText(barang.getDisposisiValidAt());

        if (holder.tgl.getText().length() > 6){
            holder.img_cek.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_check_black_24dp));
        }else {
            holder.img_cek.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_close_black_24dp));
        }
    }

    @Override
    public int getItemCount() {
        return notif_data.size();
    }

}
