package com.mandy.e_arship.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.mandy.e_arship.R;

import java.util.List;

public class adapter_notif extends RecyclerView.Adapter<adapter_notif.MyViewHolder> {

    private Context context;
    private Activity activity;
    private List<com.mandy.e_arship.model.notif.notif_data> notif_data;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView info,tgl,msg;

        public MyViewHolder(View view) {
            super(view);
            context = view.getContext();
            tgl = view.findViewById(R.id.tv_tgl);
            info = view.findViewById(R.id.tv_info);
            msg = view.findViewById(R.id.tv_msg);

        }
    }

    public void setSurat(List<com.mandy.e_arship.model.notif.notif_data> riwayat_disposisi) {
        this.notif_data = riwayat_disposisi;
        notifyDataSetChanged();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notif, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final com.mandy.e_arship.model.notif.notif_data barang = notif_data.get(position);

        holder.tgl.setText(barang.getCreate());
        holder.info.setText(barang.getTitle());
        holder.msg.setText(barang.getMsg());

    }

    @Override
    public int getItemCount() {
        return notif_data.size();
    }

}
