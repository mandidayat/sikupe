package com.mandy.e_arship.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.karan.churi.PermissionManager.PermissionManager;
import com.mandy.e_arship.R;
import com.mandy.e_arship.db.entity.Users;
import com.mandy.e_arship.db.viewModel.UsersViewModel;
import com.mandy.e_arship.model.firebaseToken.FirebaseTokenUpdate;
import com.mandy.e_arship.model.login.LoginResponse;
import com.mandy.e_arship.utility.M;
import com.mandy.e_arship.utility.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    private TextInputEditText ed_pin;
    private PermissionManager permission;
    private Button btn_login;
    private UsersViewModel usersViewModel;
    private String token;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        permission = new PermissionManager() {
            @Override
            public void ifCancelledAndCanRequest(Activity activity) {
                // Do Customized operation if permission is cancelled without checking "Don't ask again"
                // Use super.ifCancelledAndCanRequest(activity); or Don't override this method if not in use
            }

            @Override
            public void ifCancelledAndCannotRequest(Activity activity) {
                // Do Customized operation if permission is cancelled with checking "Don't ask again"
                // Use super.ifCancelledAndCannotRequest(activity); or Don't override this method if not in use
            }

            @Override
            public List<String> setPermission() {
                // If You Don't want to check permission automatically and check your own custom permission
                // Use super.setPermission(); or Don't override this method if not in use
                List<String> customPermission = new ArrayList<>();
                customPermission.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                customPermission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                customPermission.add(Manifest.permission.VIBRATE);
                customPermission.add(Manifest.permission.CAMERA);
                customPermission.add(Manifest.permission.INTERNET);
                return customPermission;
            }
        };

        permission.checkAndRequestPermissions(this);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        // Get new Instance ID token
                        token = task.getResult().getToken();

                    }
                });

        initial();
    }

    private void initial() {
        usersViewModel = new ViewModelProvider(this).get(UsersViewModel.class);
        ed_pin = findViewById(R.id.ed_pin);
        btn_login = findViewById(R.id.btn_login);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ed_pin.getText().toString().isEmpty()) {
                    post_data();
                } else {
                    Toast.makeText(LoginActivity.this, "Data tidak lengkap", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void post_data() {
        M.showLoadingDialog(this);
        Call<LoginResponse> call = RetrofitClient.getInstance().getApi().userLogin(ed_pin.getText().toString().trim());
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                M.hideLoadingDialog();
                if (response.isSuccessful()) {
                    if (response.body().getMessage().equals("Berhasil Login")) {
                        updateTokenFirebase(response);
                    } else {
                        Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Server Tidak Merespon. hubungi admin.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("res", String.valueOf(t));
                M.hideLoadingDialog();
                Toast.makeText(LoginActivity.this, "Masalah Jaringan, Coba lagi.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateTokenFirebase(final Response<LoginResponse> response) {
        M.showLoadingDialog(this);
        Call<FirebaseTokenUpdate> call = RetrofitClient.getInstance().getApi().kirim_token_firebase(response.body().getUserData().getIdUserOpd(),token);
        call.enqueue(new Callback<FirebaseTokenUpdate>() {
            @Override
            public void onResponse(Call<FirebaseTokenUpdate> call, Response<FirebaseTokenUpdate> response1) {
                M.hideLoadingDialog();
                if (response1.isSuccessful()) {
                    if (response1.body().getMessage().equals("sukses update fcm")) {
                        Users users = new Users(Integer.parseInt(response.body().getUserData().getIdUserOpd()),response.body().getUserData().getIdUserOpd(),response.body().getUserData().getTokenAkses(),response.body().getUserData().getUserOpdAn(),
                                response.body().getUserData().getUserOpdNama(),response.body().getUserData().getUserOpdTelp(),response.body().getUserData().getTokenFirebase(),response.body().getUserData().getInstansiId(),response.body().getUserData().getUserOpdStat(),
                                response.body().getUserData().getInstansiName(),response.body().getUserData().getInstansiTelepon(),response.body().getUserData().getInstansiAlamat(),response.body().getUserData().getInstansiEmail(),response.body().getUserData().getInstansiKategoriId(),
                                response.body().getUserData().getInstansiKodeUnik());

                        usersViewModel.insert(users);
                        M.setToken(response.body().getToken(),getApplicationContext());
                        M.setInstansiId(response.body().getUserData().getInstansiId(),getApplicationContext());
                        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Server Tidak Merespon. hubungi admin.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<FirebaseTokenUpdate> call, Throwable t) {
                Log.e("res", String.valueOf(t));
                M.hideLoadingDialog();
                Toast.makeText(LoginActivity.this, "Masalah Jaringan, Coba lagi.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
