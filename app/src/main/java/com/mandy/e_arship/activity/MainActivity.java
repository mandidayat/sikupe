package com.mandy.e_arship.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.mandy.e_arship.R;
import com.mandy.e_arship.db.entity.Users;
import com.mandy.e_arship.db.viewModel.UsersViewModel;
import com.mandy.e_arship.font.TextviewRoboto;
import com.mandy.e_arship.utility.SocketAplication;
import org.json.JSONObject;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private CollapsingToolbarLayout collapsingToolbarLayout;
    private FrameLayout header;
    private UsersViewModel usersViewModel;
    private List<Users> usersList;
    private TextviewRoboto nama, email;
    private Socket mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSocket = SocketAplication.get(getApplicationContext()).getSocket();
        mSocket.connect();
        mSocket.on("connection", connection);

        initial();

    }

    private void initial() {
        header = findViewById(R.id.header);
        collapsingToolbarLayout = findViewById(R.id.coolapToolbar);
        AppBarLayout appBarLayout = findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                float offsetAlpha = (appBarLayout.getY() / appBarLayout.getTotalScrollRange());

                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle("SIKUPE");
                    isShow = true;
                } else if (verticalOffset < -345) {
                    collapsingToolbarLayout.setTitle("SIKUPE");//carefull there should a space between double quote otherwise it wont work
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                } else {
                    header.setAlpha(1 - (offsetAlpha * -1));
                }
            }
        });

        nama = findViewById(R.id.nama);
        email = findViewById(R.id.jabatan);

        usersViewModel = new ViewModelProvider(this).get(UsersViewModel.class);

        usersViewModel.getAllWords().observe(this, new Observer<List<Users>>() {
            @Override
            public void onChanged(@Nullable final List<Users> words) {
                // Update the cached copy of the words in the adapter.
                assert words != null;
                if (words.size() <= 0) {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    nama.setText(words.get(0).getUser_opd_an());
                    email.setText(words.get(0).getUser_opd_nama());
                    usersList = words;
                }
            }
        });

        CardView card_keluar = findViewById(R.id.card_keluar);
        CardView card_profile = findViewById(R.id.card_profile);
        CardView card_riwayat_disposisi = findViewById(R.id.card_riwayat_disposisi);
        CardView card_registrasi = findViewById(R.id.card_registrasi);
        CardView card_riwayat_registrasi = findViewById(R.id.card_riwayat_registrasi);
        CardView card_informasi = findViewById(R.id.card_informasi);

        card_informasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, InformasiActivity.class);
                startActivity(intent);
            }
        });

        card_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });
        card_keluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usersViewModel.deleteAll();
            }
        });

        card_riwayat_disposisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RiwayatDisposisiActivity.class);
                startActivity(intent);
            }
        });

        card_registrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,RegistrasiActivity.class);
                startActivity(intent);
            }
        });

        card_riwayat_registrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,RiwayatRegistrasiActivity.class);
                startActivity(intent);
            }
        });
    }

    private Emitter.Listener connection = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            JSONObject data = (JSONObject) args[0];
            Log.e("socket", String.valueOf(data));
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mSocket.off("connection", connection);
    }

}