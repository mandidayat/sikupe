package com.mandy.e_arship.activity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.mandy.e_arship.R;
import com.mandy.e_arship.adapter.adapter_notif;
import com.mandy.e_arship.model.notif.get_notif;
import com.mandy.e_arship.utility.M;
import com.mandy.e_arship.utility.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InformasiActivity extends AppCompatActivity {
    private RecyclerView myListView;
    private com.mandy.e_arship.adapter.adapter_notif adapter_notif;
    private LinearLayout LL_reload,LL_loading,LL_no_data;
    private Button btn_refresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informasi);

        initial();
    }

    private void initial() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite, this.getTheme()));
        } else {
            toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        myListView = findViewById(R.id.rv);
        myListView.setHasFixedSize(true);
        LL_loading =  findViewById(R.id.LL_loading);
        LL_no_data =  findViewById(R.id.LL_no_data);
        LL_reload = findViewById(R.id.LL_reload);
        btn_refresh = findViewById(R.id.btn_reload);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        myListView.setLayoutManager(linearLayoutManager);
        myListView.setAdapter(adapter_notif);
        getData();

    }

    private void getData() {
        loading();
        Call<get_notif> call = RetrofitClient.getInstance().getApi().getNotif(M.getToken(getApplicationContext()));
        call.enqueue(new Callback<get_notif>() {
            @Override
            public void onResponse(Call<get_notif> call, Response<get_notif> response) {

                if (response.isSuccessful()) {
                    if (response.body().getTotalData() > 0){
                        viewData();
                        adapter_notif = new adapter_notif();
                        adapter_notif.setSurat(response.body().getNotif());
                        myListView.setAdapter(adapter_notif);
                    }else{
                        noData();
                    }
                } else {
                    reload();
                    Toast.makeText(getApplicationContext(), "Server Tidak Merespon. hubungi admin.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<get_notif> call, Throwable t) {
                Log.e("res", String.valueOf(t));
                reload();
                Toast.makeText(getApplicationContext(), "Masalah Jaringan, Coba lagi.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void loading(){
        LL_loading.setVisibility(View.VISIBLE);
        LL_reload.setVisibility(View.GONE);
        myListView.setVisibility(View.GONE);
    }

    void reload(){
        LL_loading.setVisibility(View.GONE);
        LL_reload.setVisibility(View.VISIBLE);
        myListView.setVisibility(View.GONE);
    }

    void viewData(){
        LL_loading.setVisibility(View.GONE);
        LL_reload.setVisibility(View.GONE);
        myListView.setVisibility(View.VISIBLE);
    }


    void noData(){
        LL_loading.setVisibility(View.GONE);
        LL_reload.setVisibility(View.GONE);
        myListView.setVisibility(View.GONE);
        LL_no_data.setVisibility(View.VISIBLE);
    }

}
