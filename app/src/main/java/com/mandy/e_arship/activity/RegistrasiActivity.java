package com.mandy.e_arship.activity;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mandy.e_arship.R;
import com.mandy.e_arship.model.antrian.AntrianResponse;
import com.mandy.e_arship.utility.M;
import com.mandy.e_arship.utility.RetrofitClient;
import com.mandy.e_arship.utility.SocketAplication;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class RegistrasiActivity extends AppCompatActivity {

    private Button btn_kirim;
    private TextInputEditText ed_no_surat,ed_tgl_surat,ed_perihal_surat;
    private TextInputLayout textInputLayout;
    private TextView tv_tgl;
    private Socket mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);
        mSocket = SocketAplication.get(getApplicationContext()).getSocket();
        mSocket.connect();
        initial();
    }

    private void initial() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite, this.getTheme()));
        } else {
            toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_tgl = findViewById(R.id.tv_tgl);
        btn_kirim = findViewById(R.id.btn_kirim);
        ed_no_surat = findViewById(R.id.ed_no_surat);
        ed_tgl_surat = findViewById(R.id.ed_tgl);
        ed_perihal_surat =  findViewById(R.id.ed_perihal);

        tv_tgl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show_date();
            }
        });

        btn_kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ed_no_surat.getText().toString().trim().isEmpty() || ed_perihal_surat.getText().toString().trim().isEmpty() || tv_tgl.getText().toString().trim().isEmpty()){
                    Toast.makeText(RegistrasiActivity.this, "Data tidak boleh kosong..", Toast.LENGTH_SHORT).show();
                }else {
                    kirimRegis();
                }
            }
        });
    }

    private void kirimRegis() {
        M.showLoadingDialog(this);
        Call<AntrianResponse> call = RetrofitClient.getInstance().getApi().kirim_antrian(M.getToken(getApplicationContext()),ed_no_surat.getText().toString().trim(),M.convertTgl(tv_tgl.getText().toString().trim()),ed_perihal_surat.getText().toString().trim());
        call.enqueue(new Callback<AntrianResponse>() {
            @Override
            public void onResponse(Call<AntrianResponse> call, Response<AntrianResponse> response) {
                M.hideLoadingDialog();
                if (response.isSuccessful()) {
                    if (response.body().getPesan().equals("Sukses Berhasil Submit Berkas")){
                        ed_no_surat.setText("");
                        ed_perihal_surat.setText("");
                        tv_tgl.setText("");
                        mSocket.emit("new_antrian", "new_antrian");
                        mSocket.emit("new_notif", "new_notif");
                    }
                    Toast.makeText(RegistrasiActivity.this, response.body().getPesan(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(RegistrasiActivity.this, "Server Tidak Merespon. hubungi admin.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AntrianResponse> call, Throwable t) {
                Log.e("res", String.valueOf(t));
                M.hideLoadingDialog();
                Toast.makeText(RegistrasiActivity.this, "Masalah Jaringan, Coba lagi.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void show_date(){
        Calendar newCalendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(RegistrasiActivity.this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                tv_tgl.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    private Emitter.Listener new_antrian = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            JSONObject data = (JSONObject) args[0];
            Log.e("socket", String.valueOf(data));
        }
    };

    private Emitter.Listener new_notif = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            JSONObject data = (JSONObject) args[0];
            Log.e("socket", String.valueOf(data));
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.off("new_notif", new_notif);
        mSocket.off("new_antrian", new_antrian);
    }
}
