package com.mandy.e_arship.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import com.mandy.e_arship.R;
import com.mandy.e_arship.db.entity.Users;
import com.mandy.e_arship.db.viewModel.UsersViewModel;

import java.util.List;

public class ProfileActivity extends AppCompatActivity {
    private TextView nama,jab,nama_opd,nama_karyawan,telpn,emal;
    private UsersViewModel usersViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initial();
    }

    private void initial() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite, this.getTheme()));
        } else {
            toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        usersViewModel = new ViewModelProvider(this).get(UsersViewModel.class);

        nama = findViewById(R.id.tv_nama);
        nama_karyawan = findViewById(R.id.tv_nama_2);
        nama_opd = findViewById(R.id.tv_nip);
        telpn = findViewById(R.id.tv_jabatan_2);
        emal = findViewById(R.id.tv_email);
        jab = findViewById(R.id.tv_jabatan);

        usersViewModel.getAllWords().observe(this, new Observer<List<Users>>() {
            @Override
            public void onChanged(@Nullable final List<Users> words) {
                // Update the cached copy of the words in the adapter.
                assert words != null;
                if (words.size() > 0) {
                    nama.setText(words.get(0).getUser_opd_an());
                    jab.setText(words.get(0).getUser_opd_nama());
                    nama_karyawan.setText(words.get(0).getUser_opd_an());
                    nama_opd.setText(words.get(0).getUser_opd_nama());
                    telpn.setText(words.get(0).getInstansi_telepon());
                    emal.setText(words.get(0).getInstansi_email());
                }
            }
        });
    }
}
