package com.mandy.e_arship.utility;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mandy.e_arship.R;
import com.mandy.e_arship.activity.MainActivity;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            Bundle bundle = new Bundle();
            for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
                bundle.putString(entry.getKey(), entry.getValue());
            }
            if (!bundle.isEmpty()) {
                parseRequest(bundle);
            }

        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                switch (Objects.requireNonNull(remoteMessage.getNotification().getBody())) {
                    case "Surat diproses":
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        M.showNotification(getApplicationContext(),intent,remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());
                        break;
                    case "Surat divalidasi":
                        Intent intent2 = new Intent(getApplicationContext(), MainActivity.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        M.showNotification(getApplicationContext(),intent2,remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());
                        break;
                    case "Surat Selesai":
                        Intent intent3 = new Intent(getApplicationContext(), MainActivity.class);
                        intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        M.showNotification(getApplicationContext(),intent3,remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());
                        break;
                }
            }
        }

    }

    public void parseRequest(Bundle extras) {
        M.L("MASUK case : " + extras);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            switch (Objects.requireNonNull(extras.getString("for"))){
                case "Surat diproses":
//                    if (isRunning("PermohonanCutiPegawaiActivity")) {
//                        Intent intent = new Intent("updatePermohonanCuti");
//                        intent.putExtra("model", extras);
//                        sendBroadcast(intent);
//                    }else {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        M.showNotification(getApplicationContext(),intent,extras.getString("title"),extras.getString("body"));
//                    }
                    break;
                case "Surat divalidasi":
//                    if (isRunning("RencanaKegiatanActivity")) {
//                        Intent intent = new Intent("updateRencana");
//                        intent.putExtra("model", extras);
//                        sendBroadcast(intent);
//                    }else {
                        Intent intent1 = new Intent(getApplicationContext(), MainActivity.class);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        M.showNotification(getApplicationContext(),intent1,extras.getString("title"),extras.getString("body"));
//                    }
                    break;
                case "Surat Selesai":
//                    if (isRunning("kegiatanPegawaiActivity")) {
//                        Intent intent = new Intent("updateKegiatan");
//                        intent.putExtra("model", extras);
//                        sendBroadcast(intent);
//                    }else {
                        Intent intent3 = new Intent(getApplicationContext(), MainActivity.class);
                        intent3.putExtra("body",extras.getString("body"));
                        intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        M.showNotification(getApplicationContext(),intent3,extras.getString("title"),extras.getString("body"));
//                    }
                    break;
            }
        }

    }

    @SuppressLint("NewApi")
    public boolean isRunning(String activityName) {
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(2);
        for (ActivityManager.RunningTaskInfo task : tasks) {
            if ((getPackageName() + ".activity." + activityName).equals(task.topActivity.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        sendRegistrationToServer(token);
    }

    /**
     * Schedule async work using WorkManager.
     */
    private void scheduleJob() {

    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentTitle(getString(R.string.fcm_message))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
