package com.mandy.e_arship.utility;

import com.mandy.e_arship.model.antrian.AntrianResponse;
import com.mandy.e_arship.model.firebaseToken.FirebaseTokenUpdate;
import com.mandy.e_arship.model.login.LoginResponse;
import com.mandy.e_arship.model.notif.get_notif;
import com.mandy.e_arship.model.riwayat_registtrasi.RiwayatRegistrasiResponse;
import com.mandy.e_arship.model.riwayatdisposisi.RIwayatDisposisiResponse;
import retrofit2.Call;
import retrofit2.http.*;

public interface Api {

    @GET("auth")
    Call<LoginResponse> userLogin(
            @Query("pin") String pin
    );

    @GET("disposisi")
    Call<RIwayatDisposisiResponse> getRiwayatDisposisi(
            @Header("token") String header
    );

    @GET("notif")
    Call<get_notif> getNotif(
            @Header("token") String header
    );

    @FormUrlEncoded
    @POST("antrian")
    Call<AntrianResponse> kirim_antrian(
            @Header("token") String header,
            @Field("antrian_no_surat") String antrian_no_surat,
            @Field("antrian_tgl_surat") String antrian_tgl_surat,
            @Field("antrian_perihal") String antrian_perihal
    );

    @GET("antrian")
    Call<RiwayatRegistrasiResponse> getRiwayatRegistrasi(
            @Header("token") String header,
            @Query("instansi_id") String instansi_id
    );

    @FormUrlEncoded
    @POST("auth")
    Call<FirebaseTokenUpdate> kirim_token_firebase(
            @Field("user_id") String user_id,
            @Field("token") String token
    );

}
