package com.mandy.e_arship.utility;

import android.annotation.SuppressLint;
import android.content.Context;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

public class SocketAplication {

    @SuppressLint("StaticFieldLeak")
    private static SocketAplication instance;
    private Socket socket;
    private Context context;

    public static SocketAplication get(Context context){
        if(instance == null){
            instance = getSync(context);
        }
        instance.context = context;
        return instance;
    }

    private static synchronized SocketAplication getSync(Context context){
        if (instance == null) {
            instance = new SocketAplication(context);
        }
        return instance;
    }

    public Socket getSocket(){
        return this.socket;
    }

    private SocketAplication(Context context){
        this.context = context;
        this.socket = getChatServerSocket();
//        this.friends = new ArrayList<Friend>();
    }

    private Socket getChatServerSocket(){
        try {
            socket = IO.socket(Url.base_url_socket);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        return socket;
    }

}
