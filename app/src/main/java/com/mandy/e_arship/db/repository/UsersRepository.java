package com.mandy.e_arship.db.repository;

import android.app.Application;
import android.os.AsyncTask;
import androidx.lifecycle.LiveData;
import com.mandy.e_arship.db.UsersDatabase;
import com.mandy.e_arship.db.dao.UsersDao;
import com.mandy.e_arship.db.entity.Users;

import java.util.List;

public class UsersRepository {
    private UsersDao usersDao;
    private LiveData<List<Users>> mAllUsers;

    public UsersRepository(Application application) {
        UsersDatabase db = UsersDatabase.getDatabase(application);
        usersDao = db.usersDao();
        mAllUsers = usersDao.getAll();
    }

    public LiveData<List<Users>> getAllWords() {
        return mAllUsers;
    }

    public void insert (Users word) {
        new insertAsyncTask(usersDao).execute(word);
    }

    public void deleteAll () {
        new deletetAsyncTask(usersDao).execute();
    }


    private static class insertAsyncTask extends AsyncTask<Users, Void, Void> {

        private UsersDao mAsyncTaskDao;

        insertAsyncTask(UsersDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Users... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class deletetAsyncTask extends AsyncTask<Users, Void, Void> {

        private UsersDao mAsyncTaskDao;

        deletetAsyncTask(UsersDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Users... params) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }

}
