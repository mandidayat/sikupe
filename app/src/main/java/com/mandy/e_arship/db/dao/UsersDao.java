package com.mandy.e_arship.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.*;
import com.mandy.e_arship.db.entity.Users;

import java.util.List;

@Dao
public interface UsersDao {
    @Query("SELECT * FROM users")
    LiveData<List<Users>> getAll();

    @Query("SELECT * FROM users WHERE id_user_opd IN (:userIds)")
    List<Users> loadAllByIds(int[] userIds);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Users word);

    @Query("DELETE FROM users")
    void deleteAll();
}
