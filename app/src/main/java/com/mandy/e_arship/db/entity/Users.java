package com.mandy.e_arship.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "users")
public class Users {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "id_user_opd")
    private String id_user_opd;
    @ColumnInfo(name = "token_akses")
    private String token_akses;
    @ColumnInfo(name = "user_opd_an")
    private String user_opd_an;
    @ColumnInfo(name = "user_opd_nama")
    private String user_opd_nama;
    @ColumnInfo(name = "user_opd_telp")
    private String user_opd_telp;
    @ColumnInfo(name = "token_firebase")
    private String token_firebase;
    @ColumnInfo(name = "instansi_id")
    private String instansi_id;
    @ColumnInfo(name = "user_opd_stat")
    private String user_opd_stat;
    @ColumnInfo(name = "instansi_name")
    private String instansi_name;
    @ColumnInfo(name = "instansi_telepon")
    private String instansi_telepon;
    @ColumnInfo(name = "instansi_alamat")
    private String instansi_alamat;
    @ColumnInfo(name = "instansi_email")
    private String instansi_email;
    @ColumnInfo(name = "instansi_kategori_id")
    private String instansi_kategori_id;
    @ColumnInfo(name = "instansi_kode_unik")
    private String instansi_kode_unik;

    public Users(int id, String id_user_opd, String token_akses, String user_opd_an, String user_opd_nama, String user_opd_telp, String token_firebase, String instansi_id, String user_opd_stat, String instansi_name, String instansi_telepon, String instansi_alamat, String instansi_email, String instansi_kategori_id, String instansi_kode_unik) {
        this.id = id;
        this.id_user_opd = id_user_opd;
        this.token_akses = token_akses;
        this.user_opd_an = user_opd_an;
        this.user_opd_nama = user_opd_nama;
        this.user_opd_telp = user_opd_telp;
        this.token_firebase = token_firebase;
        this.instansi_id = instansi_id;
        this.user_opd_stat = user_opd_stat;
        this.instansi_name = instansi_name;
        this.instansi_telepon = instansi_telepon;
        this.instansi_alamat = instansi_alamat;
        this.instansi_email = instansi_email;
        this.instansi_kategori_id = instansi_kategori_id;
        this.instansi_kode_unik = instansi_kode_unik;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getId_user_opd() {
        return id_user_opd;
    }

    public String getToken_akses() {
        return token_akses;
    }

    public String getUser_opd_an() {
        return user_opd_an;
    }

    public String getUser_opd_nama() {
        return user_opd_nama;
    }

    public String getUser_opd_telp() {
        return user_opd_telp;
    }

    public String getToken_firebase() {
        return token_firebase;
    }

    public String getInstansi_id() {
        return instansi_id;
    }

    public String getUser_opd_stat() {
        return user_opd_stat;
    }

    public String getInstansi_name() {
        return instansi_name;
    }

    public String getInstansi_telepon() {
        return instansi_telepon;
    }

    public String getInstansi_alamat() {
        return instansi_alamat;
    }

    public String getInstansi_email() {
        return instansi_email;
    }

    public String getInstansi_kategori_id() {
        return instansi_kategori_id;
    }

    public String getInstansi_kode_unik() {
        return instansi_kode_unik;
    }
}
