package com.mandy.e_arship.db.viewModel;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import com.mandy.e_arship.db.entity.Users;
import com.mandy.e_arship.db.repository.UsersRepository;

import java.util.List;

public class UsersViewModel extends AndroidViewModel {

    private UsersRepository mRepository;
    private LiveData<List<Users>> mAllUsers;

    public UsersViewModel(@NonNull Application application) {
        super(application);
        mRepository = new UsersRepository(application);
        mAllUsers = mRepository.getAllWords();
    }

    public LiveData<List<Users>> getAllWords() { return mAllUsers; }

    public void insert(Users word) { mRepository.insert(word); }

    public void deleteAll() { mRepository.deleteAll(); }

}
