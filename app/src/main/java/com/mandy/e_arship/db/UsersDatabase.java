package com.mandy.e_arship.db;

import android.content.Context;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import com.mandy.e_arship.db.dao.UsersDao;
import com.mandy.e_arship.db.entity.Users;


@Database(entities = Users.class, version = 1)
public abstract class UsersDatabase extends RoomDatabase {

    public abstract UsersDao usersDao();
    private static volatile UsersDatabase INSTANCE;

    public static UsersDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (UsersDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            UsersDatabase.class, "word_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final UsersDao mDao;

        PopulateDbAsync(UsersDatabase db) {
            mDao = db.usersDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
//            mDao.deleteAll();
//            Users word = new Users(1,"1","1","1","1","1","1","1","1","1","1","1","1",
//                    "1","1");
//            mDao.insert(word);

            return null;
        }
    }
}
